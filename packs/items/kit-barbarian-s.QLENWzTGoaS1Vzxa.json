{
  "_id": "QLENWzTGoaS1Vzxa",
  "name": "Kit, Barbarian's",
  "type": "container",
  "img": "icons/containers/chest/chest-reinforced-steel-red.webp",
  "system": {
    "inventoryItems": [
      {
        "name": "Backpack",
        "type": "container",
        "img": "systems/pf1/icons/items/inventory/backpack.jpg",
        "system": {
          "description": {
            "value": "<p>This leather knapsack has one large pocket that closes with a buckled strap and holds about 2 cubic feet of material. Some may have one or more smaller pockets on the sides.</p>"
          },
          "weight": {
            "value": 2
          },
          "price": 2
        },
        "flags": {
          "core": {
            "sourceId": "Compendium.pf1.items.lSCPUK5Ea6R0t4fz"
          }
        },
        "_id": "iydUHCQPJd7VyHmV"
      },
      {
        "name": "Pouch, Belt",
        "type": "loot",
        "img": "icons/containers/bags/coinpouch-simple-leather-tan.webp",
        "system": {
          "description": {
            "value": "<p class=\"source\"><b>Source</b> <em>PZO1110</em></p>\n<p>A belt pouch is crafted of soft cloth or leather. They typically hold up to 10 lb. or 1/5 cubic ft. of items.</p>\n<p><b>Empty Weight</b>: 1/2 lb.<sup>1</sup> <b>Capacity</b>: 1/5 cubic ft./10 lb.<sup>1</sup></p>\n<p class=\"source\"><sup>1</sup> When made for Medium characters. Weighs one-quarter the normal amount when made for Small characters. Weighs twice the normal amount when made for Large characters. Containers carry one-quarter the normal amount when made for Small characters.</p>"
          },
          "weight": {
            "value": 0.5
          },
          "price": 1,
          "equipped": false,
          "subType": "gear"
        },
        "flags": {
          "core": {
            "sourceId": "Compendium.pf1.items.wjvxmolafiegvesl"
          }
        },
        "_id": "Id7vi9aUaTSQ3soZ"
      },
      {
        "name": "Flint And Steel",
        "type": "loot",
        "img": "icons/commodities/stone/rock-chunk-grey.webp",
        "system": {
          "description": {
            "value": "<p>Lighting a <em>torch</em> with a <em>flint and steel</em> is a full-round action. Lighting any other fire with them takes at least that long.</p>"
          },
          "price": 1,
          "equipped": false,
          "subType": "gear"
        },
        "flags": {
          "core": {
            "sourceId": "Compendium.pf1.items.ygmrcisvftnvjyob"
          }
        },
        "_id": "JfripS5g4rft6Lzp"
      },
      {
        "name": "Soap",
        "type": "consumable",
        "img": "systems/pf1/icons/items/inventory/soap.jpg",
        "system": {
          "description": {
            "value": "<p class=\"source\"><b>Source</b> <em>PPZO9410</em></p>\n<p>You can use this thick block of soap to scrub clothes, pots, linens, or anything else that might be dirty. A bar of soap has approximately 50 uses.</p>"
          },
          "weight": {
            "value": 0.5
          },
          "price": 0.01,
          "actions": [
            {
              "_id": "ootoocddr3l2eoat",
              "name": "Use",
              "img": "systems/pf1/icons/items/inventory/soap.jpg",
              "activation": {
                "type": "special",
                "unchained": {
                  "type": "special"
                }
              },
              "actionType": ""
            }
          ],
          "uses": {
            "per": "charges",
            "value": 50,
            "maxFormula": "50"
          },
          "tag": "soap",
          "subType": "misc"
        },
        "flags": {
          "core": {
            "sourceId": "Compendium.pf1.items.yMoclCylbCRKyaZF"
          }
        },
        "_id": "EoTOs2bFyutxXsyq"
      },
      {
        "name": "Torch",
        "type": "loot",
        "img": "systems/pf1/icons/items/inventory/torch.jpg",
        "system": {
          "description": {
            "value": "<p><b>Price</b> 1 cp; <b>Weight</b> 1 lb.</p>\n<p>A torch burns for 1 hour, shedding normal light in a 20-foot radius and increasing the light level by one step for an additional 20 feet beyond that area (darkness becomes dim light and dim light becomes normal light). A torch does not increase the light level in normal light or bright light. If a torch is used in combat, treat it as a one-handed improvised weapon that deals bludgeoning damage equal to that of a gauntlet of its size, plus 1 point of fire damage.</p>"
          },
          "weight": {
            "value": 1
          },
          "price": 0.01,
          "equipped": false,
          "subType": "gear"
        },
        "flags": {
          "core": {
            "sourceId": "Compendium.pf1.items.snfogneawzopfzzl"
          }
        },
        "_id": "69hoa9wCmf0e1Dsh"
      },
      {
        "name": "Waterskin",
        "type": "loot",
        "img": "systems/pf1/icons/items/inventory/waterskin.jpg",
        "system": {
          "description": {
            "value": "<p class=\"source\"><b>Source</b> <em>PZO1110</em></p>\n<p>A water or wineskin holds 1/2 gallon of liquid and weighs 4 lb when full.</p>\n<p><b>Empty Weight</b>: -; <b>Capacity</b>: 1/2 gallon/4 lb.<sup>1</sup></p>\n<p class=\"source\"><sup>1</sup> When made for Medium characters. Weighs one-quarter the normal amount when made for Small characters. Weighs twice the normal amount when made for Large characters. Containers carry one-quarter the normal amount when made for Small characters.</p>\n<h3>Magic Containers</h3>\n<p>Magic containers come in almost as many varieties as there are <em>alchemists</em>, and so are of inestimable value to <em>alchemists</em> with specific needs. Some of the most well-known magic containers are described here.</p>\n<p>Because of a varied nature of alchemical items and their effects, not all combinations of alchemical items and magic bottles are viable or even make sense, even though they technically might be allowed by the rules. The GM should have the final say on whether or not a particular alchemical item can function within one of the magical containers listed in this section.</p>\n<table>\n<thead>\n<tr>\n<th colspan=\"4\">Magic Containers</th>\n</tr>\n<tr>\n<th>Item</th>\n<th>Price</th>\n<th>Weight</th>\n<th>Source</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<td><i>Focusing Flask</i></td>\n<td>700 gp</td>\n<td>1 lb.</td>\n<td class=\"source text\"><em>PPC:AM</em></td>\n</tr>\n<tr>\n<td><i>Retort of Control</i></td>\n<td>13,000 gp</td>\n<td>-</td>\n<td class=\"source text\"><em>PPC:AM</em></td>\n</tr>\n<tr>\n<td><i>Vial of Efficacious Medicine</i></td>\n<td>7,000 gp</td>\n<td>-</td>\n<td class=\"source text\"><em>PPC:AM</em></td>\n</tr>\n<tr>\n<td><i>Winged Bottle</i></td>\n<td>1,620 gp</td>\n<td>1 lb.</td>\n<td class=\"source text\"><em>PPC:AM</em></td>\n</tr>\n</tbody>\n</table>"
          },
          "weight": {
            "value": 4
          },
          "price": 1,
          "equipped": false,
          "subType": "gear"
        },
        "flags": {
          "core": {
            "sourceId": "Compendium.pf1.items.bsztkhkrhgmsbqpt"
          }
        },
        "_id": "aXVwewD695yM613Y"
      },
      {
        "name": "Rations, Trail",
        "type": "loot",
        "img": "systems/pf1/icons/items/inventory/meat.jpg",
        "system": {
          "description": {
            "value": "<h4>Rations, Trail</h4>\n<p><b>Price</b> 5 sp; <b>Weight</b> 1 lb.</p>\n<p>The listed price is for a day's worth of food. This bland food is usually some kind of hard tack, jerky, and dried fruit, though the contents vary from region to region and the race of those creating it. As long as it stays dry, it can go for months without spoiling.</p>"
          },
          "weight": {
            "value": 1
          },
          "price": 0.5,
          "equipped": false,
          "subType": "gear"
        },
        "flags": {
          "core": {
            "sourceId": "Compendium.pf1.items.manvtbznrwjmnrua"
          }
        },
        "_id": "tUrpsxmqdYcnz3UO"
      },
      {
        "name": "Blanket",
        "type": "loot",
        "img": "systems/pf1/icons/items/inventory/dice.jpg",
        "system": {
          "description": {
            "value": "<h4>Blanket</h4>\n<p class=\"source\"><b>Source</b> <em>PZO1115</em></p>\n<p><b>Price</b> 5 sp; <b>Weight</b> 3 lbs.</p>\n<p>This warm, woven blanket has straps so it can be rolled up and tied. Blankets are often used in conjunction with bedrolls to provide additional warmth or a ground cushion.</p>\n<p><b>Winter blanket</b> This is a fur blanket large enough for one person. <b>Source</b> <em>PZO1110</em></p>"
          },
          "weight": {
            "value": 3
          },
          "price": 0.5,
          "equipped": false,
          "subType": "gear"
        },
        "flags": {
          "core": {
            "sourceId": "Compendium.pf1.items.fwfwshvyweqbthwk"
          }
        },
        "_id": "BssAtLRnrP0EbcN1"
      },
      {
        "name": "Rope",
        "type": "loot",
        "img": "systems/pf1/icons/items/inventory/dice.jpg",
        "system": {
          "description": {
            "value": "<h4>Rope</h4>\n<p><b>Hempen</b> The DC to escape hemp rope bonds is equal to 20 + the CMB of the creature that tied the bonds. Ropes do not need to make a check every round to maintain the pin. If the DC to escape is higher than 20 + the tying creatures CMB, the tied up creature cannot escape from the bonds, even with a natural 20 on the check. This rope has 2 hit points and can be burst with a DC 23 Strength check. <b>Price</b> 1 gp; <b>Weight</b> 10 lbs.</p>\n<p><b>FYI</b>: The <em>Equipment Trick</em> feat provides a number of options for using this item in combat.</p>\n<p><b>Silk Rope</b> This 50-foot length of silk rope has 4 hit points and can be broken with a DC 24 Strength check. <b>Price</b> 10 gp; <b>Weight</b> 5 lbs.</p>\n<p><b>Bloodvine Rope</b> This 50-foot length of tough, lightweight rope is made from alchemically treated bloodvine, a rare scarlet-colored vine that grows only in warm jungle environments. Though prized by climbers for its durability, bloodvine can also be used to bind creatures. Bloodvine rope has a <em>hardness</em> of 5 and 10 <em>hit points</em>, and can be broken with a DC 30 <em>Strength</em> check. A creature bound by bloodvine rope can escape with a DC 35 <em>Escape Artist</em> check or a DC 30 <em>Strength</em> check. The DC to create bloodvine rope with <em>Craft</em> (alchemy) is 30. <b>Price</b> 200 gp; <b>Weight</b> 5 lbs. <b>Source</b> <em>PRG:ACG</em></p>\n<p><b>Spider's Silk Rope</b> This 50-foot length of rope is woven of strands of silk from monstrous spiders. Rare to virtually nonexistent on the surface world, it is commonly used by the dark elves, though shorter spider's silk rope scraps (generally no more than 10 feet long) occasionally appear among goblins. Spider's silk rope has 6 hit points and can be broken with a DC 25 Strength check. <b>Price</b> 100 gp; <b>Weight</b> 4 lbs.</p>"
          },
          "weight": {
            "value": 10
          },
          "price": 1,
          "equipped": false,
          "subType": "gear"
        },
        "flags": {
          "core": {
            "sourceId": "Compendium.pf1.items.qlmtnppwxkubqyhp"
          }
        },
        "_id": "jd7f0W5umTSnd0ZB"
      },
      {
        "name": "Pot, Cooking (Iron)",
        "type": "loot",
        "img": "icons/tools/cooking/cauldron-empty.webp",
        "system": {
          "description": {
            "value": "<p class=\"source\"><b>Source</b> <em>PZO1110</em></p>\n<p>Cooking pots come in a variety of materials, but the most common is formed of iron.</p>\n<p>A mithral cooking pot weighs 2 lbs. and costs 2,001 gp.</p>\n<p><b>Empty Weight</b>: 2 lb.; <b>Capacity</b>: 1 gallon/8 lb.</p>"
          },
          "weight": {
            "value": 4
          },
          "price": 0.8,
          "equipped": false,
          "subType": "gear"
        },
        "flags": {
          "core": {
            "sourceId": "Compendium.pf1.items.habofoapdifirevm"
          }
        },
        "_id": "SvHldYT3WgB3JmdL"
      }
    ]
  }
}
