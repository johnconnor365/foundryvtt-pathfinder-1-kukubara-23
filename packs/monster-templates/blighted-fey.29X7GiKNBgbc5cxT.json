{
  "_id": "29X7GiKNBgbc5cxT",
  "name": "Blighted Fey",
  "type": "feat",
  "img": "systems/pf1/icons/skills/violet_07.jpg",
  "system": {
    "description": {
      "value": "<p><b>Acquired/Inherited Template</b> Acquired<br><b>Simple Template</b> No<br><b>Usable with Summons</b> No<p>In certain wilderness regions, strange corruptions of nature fester and grow where the boundaries between this world and the Abyss grow thin. Dangerous and evil fungal creatures rise to power in these blighted reaches, such as sinister <a href=\"https://aonprd.com/MonsterDisplay.aspx?ItemName=Fungus%20Queen\">fungus queens</a> or legions of undead spore zombies, but when fey creatures become infused with this corruption and are themselves blighted, the resulting monstrosities are particularly vile.<p>The typical Abyssal blight manifests as a black and greasy fungal rot that sways tree branches and limbs where no wind propels them, and a mystic network of fell power extends an unnatural awareness between nearby blighted fey. Dryads are often the most insidious of these corrupted fey; they lure humanoids to literally and spiritually dark places to beget more daughters from their dark embrace— and further spread the disease. The dryads connect through a unified but tainted mystic field that transcends their ordinary limitations and permits them to treat all infected trees as their own bonded trees.<p>Only magic such as <a href=\"https://aonprd.com/SpellDisplay.aspx?ItemName=miracle\">miracle</a>,<a href=\"https://aonprd.com/SpellDisplay.aspx?ItemName=%20limited%20wish\"> limited wish</a>, or <a href=\"https://aonprd.com/SpellDisplay.aspx?ItemName=wish\">wish</a> can sever the connection to the Abyss and cure a blighted fey, restoring the creature to its uncorrupted state if it fails to resist the transformation with a successful Will saving throw against the spell in question. Of course, once the blight takes hold, a fey creature is corrupted not only in body but in mind as well, and any attempts to cure such a fey creature are bound to be met with violence.<p>The process of creating a blighted fey can vary. In some cases, the transformation requires a new fey creature to be bound in blighted fungal tendrils and to languish within the corrupted region for 24 hours, but in other cases the blight might affect a creature almost instantaneously. The potential to resist such corruption with a successful saving throw varies, as does the DC to resist such an effect. A <a href=\"https://aonprd.com/SpellDisplay.aspx?ItemName=wish\">wish</a>, <a href=\"https://aonprd.com/SpellDisplay.aspx?ItemName=limited%20wish\">limited wish</a>, or <a href=\"https://aonprd.com/SpellDisplay.aspx?ItemName=miracle\">miracle</a> can transform a fey creature into a blighted fey in much the same way this magic can be used to rescue a blighted creature from its corruption.<p>Although the ooze creatures known as blights share a name with the planar corruption that creates blighted fey, these two types of creatures do not get along. In fact, blights often see the advance of an Abyssal incursion into their natural territory as much of an affront as the encroachment of mortal civilization, and in such cases those who oppose either group can sometimes find allies in the enemies of their enemy. Blights are usually the more difficult of the two to reach an accord with, but one should always tread with caution when dealing with the blighted fey.<p>“Blighted fey” is an acquired template that can be added to any fey creature with 2 or more Hit Dice, referred to hereafter as the base creature. A blighted fey uses the base creature’s statistics and abilities except as noted here.<p><b>CR:</b> Base creature’s CR + 2.<p><b>Alignment:</b> Chaotic evil.<p><b>Senses:</b> A blighted fey gains darkvision to a range of 60 feet if the base creature did not already have it. If the base creature already has darkvision, the ability is extended by an additional 30 feet.<p><b>Armor Class:</b> Natural armor improves by 2.<p><b>Defensive Abilities:</b> A blighted fey gains DR 10/cold iron and good; immunity to disease, paralysis, poison, and polymorph; and resistance to cold 10 and electricity 10. A blighted fey also gains spell resistance equal to 11 + its newly adjusted CR.<p>Additionally, a blighted fey gains the following ability.<p><em>Fungal Rejuvenation (Su):</em> So long as it remains within 300 yards of any blighted plant of significant size (such as a tree) and remains standing on moist earth, the blighted fey gains fast healing 5. The infected tree does not need to be specifically bonded to the blighted fey for this ability to function.<p><b>Special Attacks:</b> A blighted fey gains the following special attacks. Unless otherwise noted, save DCs are equal to 10 + half the blighted fey’s Hit Dice + the blighted fey’s Constitution modifier.<p><em>Parasitic Bond (Su):</em> Once per day with a successful thorn throw attack (see below), the blighted fey can transform the thorns into a single burrowing, wriggling maggot that infests and infuses the target with a short-term curse from within unless the target succeeds at a Fortitude save. This parasite creates an unholy link to the target, binding it to the blighted fey. This binding persists for 5 rounds, during which half of any hit point damage taken by the blighted fey is dealt to the linked target instead. The type of damage remains consistent with the damage dealt to the blighted fey. Only one parasitic bond can be maintained with one creature at a time. This bond is a curse and a disease effect.<p><em>Thorn Throw (Ex):</em> A blighted fey can shoot a fistful of needle-sharp thorns at a single target within 20 feet as a standard action. The thorn attack deals an amount of damage equal to a sting natural attack, with damage based on the blighted fey’s size (1d4 for a Medium fey), except the attack is resolved as a thrown ranged attack instead of a melee attack. Thorn throw is always a primary attack.<p><b>Special Qualities:</b> A blighted fey gains the following special qualities.<p><em>Blighted Unity (Su):</em> Blighted fey within 100 feet of one other can communicate through a shared fungal mind. This does not permit blighted fey to see and hear through each other’s senses, but they can share specific situational information and tactics through telepathy. If one blighted fey within range is aware of danger, they are all aware of danger and cannot be surprised.<p><em>Daughter of the Blight (Su)</em>: Fey creatures who normally have close ties to a specific plant gain this specific quality. For example, blighted fey <a href=\"https://aonprd.com/MonsterDisplay.aspx?ItemName=Dryad\">dryads</a> are no longer dependent upon a specific tree. A dryad’s tree dependent special ability is modified (but not replaced) so blighted fey dryads are required only to remain within 300 yards of any blighted tree. This ability applies only to dryads and other fey who bond with plants in a similar manner.<p><em>Tainted Blood (Ex):</em> A blighted fey’s blood and flesh are rife with disease. Any creature that deals damage with a bite attack against a blighted fey, swallows one whole, or otherwise ingests part of one must succeed at a Fortitude save (as per Special Attacks above) or take 1 point of Strength damage and 1 point of Dexterity damage. One minute later, the affected creature must succeed at a second save at the same DC or be nauseated for 1 minute and take 1d6 points of Strength damage and 1d6 points of Dexterity damage. This is a disease effect.<p><b>Ability Scores:</b> Str +4, Con +4, Cha +2.<p><b>Feats:</b> Blighted fey gain <a href=\"https://aonprd.com/FeatDisplay.aspx?ItemName=Toughness\">Toughness</a> as a bonus feat.<p><b>Skills:</b> A blighted fey gains a +2 racial bonus on Knowledge (nature), Perception, and Stealth checks.</p>"
    },
    "changes": [
      {
        "_id": "is3h94db",
        "formula": "2",
        "subTarget": "nac",
        "modifier": "untyped"
      },
      {
        "_id": "i6sipaz2",
        "formula": "4",
        "subTarget": "str",
        "modifier": "untyped"
      },
      {
        "_id": "7vwt8ymv",
        "formula": "4",
        "subTarget": "con",
        "modifier": "untyped"
      },
      {
        "_id": "6bufyppp",
        "formula": "2",
        "subTarget": "cha",
        "modifier": "untyped"
      },
      {
        "_id": "8hvxr882",
        "formula": "2",
        "subTarget": "skill.kna",
        "modifier": "untyped"
      },
      {
        "_id": "9bx9alkp",
        "formula": "2",
        "subTarget": "skill.per",
        "modifier": "untyped"
      },
      {
        "_id": "gftmoy63",
        "formula": "2",
        "subTarget": "skill.ste",
        "modifier": "untyped"
      }
    ],
    "subType": "template",
    "crOffset": "2"
  }
}
